# p2p_tool

#### 介绍

采用Go语言编写的支持各种P2P协议的下载工具

#### 软件架构

- 项目架构(GUI): [Wails V2](https://wails.io/) 它提供了将 Go 代码和 Web 前端(WebView2 运行时 for window)一起打包成单个二进制文件的能力
- Go库: [torrent](https://github.com/anacrolix/torrent)
- 用户界面库: [React](https://zh-hans.reactjs.org/)
- 用户界面语言: [Typescript](https://www.tslang.cn/docs/home.html)
- UI组件库: [MUI](https://mui.com/zh/getting-started/usage/)
- 图标库: [css.gg](https://css.gg/)
#### 安装教程

#### 使用说明

#### 参考资料

- golang-standards/project-layout: https://github.com/golang-standards/project-layout/blob/master/README_zh.md

##### P2P protocol

- http://www.ruanyifeng.com/blog/2009/11/future_of_bittorrent.html
- https://justjavac.com/other/2015/02/01/bittorrent-dht-protocol.html
- https://zh.wikipedia.org/wiki/%E7%A3%81%E5%8A%9B%E9%93%BE%E6%8E%A5
- http://qtchina.github.io/4/node_419.html
- https://www.cxymm.net/article/u012785382/70674875
- https://mojotv.cn/go/golang-torrent
- https://github.com/veggiedefender/torrent-client/
- https://www.aneasystone.com/archives/2015/05/analyze-magnet-protocol-using-wireshark.html

##### P2P libraries

- torrent: https://github.com/anacrolix/torrent
- BitTorrent DHT: https://github.com/boramalper/magnetico
- https://github.com/BrightStarry/zx-bt
- https://github.com/shiyanhui/dht/blob/master/README_CN.md

